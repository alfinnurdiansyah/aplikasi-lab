package com.sourcey.materiallogindemo.adapter;

import android.support.v4.app.Fragment;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.sourcey.materiallogindemo.R;
import com.sourcey.materiallogindemo.item.itemkelas;

import java.util.ArrayList;

public class ListJadwalAdapter extends RecyclerView.Adapter<ListJadwalAdapter.CardViewViewHolder>{
    private Context context;
    private Fragment fragment;
    private ArrayList<itemkelas> listkelas;
    private ArrayList<itemkelas> getListkelas() {
        return listkelas;
    }
    public void setListkelas(ArrayList<itemkelas> listkelas) {
        this.listkelas = listkelas;
    }
    public ListJadwalAdapter(Context context, Fragment fragment) {
        this.fragment= fragment;
        this.context = context;
    }
    @NonNull
    @Override
    public CardViewViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_jadwal_user, viewGroup, false);
        return new CardViewViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CardViewViewHolder holder, int position) {
        itemkelas p = getListkelas().get(position);
        holder.tvhari.setText(p.getHari());
        holder.tvnamamatkul.setText(p.getMatkul());
        holder.tvkelas.setText(p.getKelas());
        holder.tvruang.setText(p.getRuang());
        holder.tvjam.setText(p.getJam());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentIntegrator scannerIntegrator = IntentIntegrator.forSupportFragment(fragment);
  //              intentIntegrator.forSupportFragment;
                scannerIntegrator.setPrompt("Scan barcode");
                scannerIntegrator.setBeepEnabled(false);
                scannerIntegrator.setOrientationLocked(true);
                scannerIntegrator.initiateScan();
            }
        });

    }

    @Override
    public int getItemCount() {
        return getListkelas().size();
    }

    class CardViewViewHolder extends RecyclerView.ViewHolder{
        TextView tvkelas, tvhari,tvnamamatkul,tvruang,tvjam;
        CardViewViewHolder(View itemView) {
            super(itemView);
            tvkelas=itemView.findViewById(R.id.tv_nama_kelas);
            tvhari=itemView.findViewById(R.id.tv_hari);
            tvnamamatkul=itemView.findViewById(R.id.tv_nama_matkul);
            tvruang=itemView.findViewById(R.id.tv_ruangan);
            tvjam=itemView.findViewById(R.id.tv_jam);
        }
    }

}
