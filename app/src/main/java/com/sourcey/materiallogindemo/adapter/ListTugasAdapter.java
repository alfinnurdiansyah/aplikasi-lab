package com.sourcey.materiallogindemo.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sourcey.materiallogindemo.R;
import com.sourcey.materiallogindemo.item.itemtugas;

import java.util.ArrayList;

public class ListTugasAdapter extends RecyclerView.Adapter<ListTugasAdapter.CardViewViewHolder>{
    private Context context;
    private ArrayList<itemtugas> listtugas;
    private ArrayList<itemtugas> getListtugas() {
        return listtugas;
    }
    public void setListkelas(ArrayList<itemtugas> listtugas) {
        this.listtugas = listtugas;
    }
    public ListTugasAdapter(Context context) {
        this.context = context;
    }
    @NonNull
    @Override
    public CardViewViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_tugas_user, viewGroup, false);
        return new CardViewViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CardViewViewHolder holder, int position) {
        itemtugas p = getListtugas().get(position);
        holder.deskripsi.setText(p.getDeskripsi());
        holder.deadline.setText(p.getDeadline());
        holder.namakelas.setText(p.getNamakelas());
        holder.namatugas.setText(p.getNamatugas());
        holder.kumpulkan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return getListtugas().size();
    }

    class CardViewViewHolder extends RecyclerView.ViewHolder{
        TextView namatugas, namakelas,deadline,deskripsi,kumpulkan;
        CardViewViewHolder(View itemView) {
            super(itemView);
            kumpulkan=itemView.findViewById(R.id.kumpulkantugas);
            namatugas=itemView.findViewById(R.id.nama_tugas);
            namakelas=itemView.findViewById(R.id.namakelas);
            deadline=itemView.findViewById(R.id.deadline);
            deskripsi=itemView.findViewById(R.id.deskripsi);
        }
    }
}
