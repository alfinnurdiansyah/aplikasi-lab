package com.sourcey.materiallogindemo;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;


public class MainActivity extends AppCompatActivity {
    private UserPreference mUserPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        mUserPreference= new UserPreference(MainActivity.this);
        final Boolean firstRun = mUserPreference.getFirstRun();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(firstRun){
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                    finish();
                }else{
                  Intent intent = new Intent(getApplicationContext(), MenuUtama.class);
                  startActivity(intent);
                  finish();
                }
            }
        }, 3000L); //3000 L = 3 detik
    }

}
