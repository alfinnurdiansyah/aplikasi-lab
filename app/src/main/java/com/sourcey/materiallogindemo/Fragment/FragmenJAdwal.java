package com.sourcey.materiallogindemo.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.sourcey.materiallogindemo.R;
import com.sourcey.materiallogindemo.adapter.ListJadwalAdapter;
import com.sourcey.materiallogindemo.item.itemkelas;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmenJAdwal extends Fragment {
    private RecyclerView rvCategory;
    private ArrayList<itemkelas> list = new ArrayList<>();
    public FragmenJAdwal() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_fragmen_jadwal, container, false);
        getActivity().setTitle("Jadwal");
        rvCategory = view.findViewById(R.id.rv_category);
        rvCategory.setHasFixedSize(true);
        itemkelas itemkelass=new itemkelas();
        itemkelass.setHari("Senin");
        itemkelass.setKelas("A");
        itemkelass.setJam("07.00 - 08.30");
        itemkelass.setMatkul("Praktikum Alpro");
        itemkelass.setRuang("Labbis");
        list.add(itemkelass);
        showRecyclerCardView();
        return view;
    }
    private void showRecyclerCardView(){
        rvCategory.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
        ListJadwalAdapter cardViewPresidentAdapter = new ListJadwalAdapter(getActivity().getApplicationContext(),FragmenJAdwal.this);
        cardViewPresidentAdapter.setListkelas(list);
        rvCategory.setAdapter(cardViewPresidentAdapter);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null){
            if (result.getContents() == null){
                Toast.makeText(getActivity().getApplicationContext(), "Hasil tidak ditemukan", Toast.LENGTH_SHORT).show();
            }else{
                list.get(0).setRuang(result.getContents());
                showRecyclerCardView();
                Toast.makeText(getActivity().getApplicationContext(), result.getContents(), Toast.LENGTH_SHORT).show();
            }
        }else{
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
